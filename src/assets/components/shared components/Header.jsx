import React from 'react';
import { useState } from 'react';
import { Link } from 'react-router-dom';
import LOGO from '../../images/LOGO.png';
import { MdDarkMode } from "react-icons/md";
import { MdOutlineLightMode } from "react-icons/md";
import { IconButton } from "@material-tailwind/react";
import photo9 from '../../images/photo9.png';

const Header = () => {

     const [open, setOpen] = useState(false);
     const [dark, setDark] = useState(false);

     return (
          <header className={` dark:bg-darkBg h-[10vh] bg-bg  z-20 flex w-full items-center md:px-20 px-8`}>
               <div className="container">
                    <div className="relative -mx-4 flex items-center justify-between ">
                         <div className="w-60 max-w-full px-4 ">
                              {
                                   dark ? <Link href="/" className="block w-full">
                                   <img
                                        src={photo9}
                                        alt="logo"
                                        className="w-14 md:w-20"
                                   />
                              </Link>:<Link href="/" className="block w-full">
                                   <img
                                        src={LOGO}
                                        alt="logo"
                                        className="w-14 md:w-20"
                                   />
                              </Link>
                              }
                              
                         </div>
                         <div className="flex w-full items-center justify-around px-4 ">
                              <div>
                                   <button
                                        onClick={() => setOpen(!open)}
                                        id="navbarToggler"
                                        className={` ${open && "dark:bg-darkBg z-40 navbarTogglerActive"
                                             } absolute right-4 top-1/2 block -translate-y-1/2 rounded-lg px-3 py-[6px] ring-primary focus:ring-2 lg:hidden`}
                                   >
                                        <span className="z-40 relative my-[6px] block h-[2px] w-[30px] bg-blue-gray-900 dark:bg-white"></span>
                                        <span className="z-40 relative my-[6px] block h-[2px] w-[30px] bg-blue-gray-900 dark:bg-white"></span>
                                        <span className="z-40 relative my-[6px] block h-[2px] w-[30px] bg-blue-gray-900 dark:bg-white"></span>
                                   </button>
                                   <nav
                                        id="navbarCollapse"
                                        className={` z-40 absolute right-4 top-full w-full max-w-[250px] rounded-lg dark:bg-darkBg bg-bg px-6 py-5 shadow dark:bg-dark-2 lg:static lg:block lg:w-full lg:max-w-full lg:bg-transparent lg:shadow-none lg:dark:bg-transparent ${!open && "hidden"
                                             } `}
                                   >
                                        <ul className="block lg:flex items-center justify-center">
                                             <ListItem NavLink="/">ACCUEIL</ListItem>
                                             <ListItem NavLink="/about">A PROPOS</ListItem>
                                             <ListItem NavLink="/services">SERVICES</ListItem>
                                             <ListItem NavLink="/devis">OBTENEZ UN DEVIS</ListItem>
                                        </ul>
                                   </nav>
                              </div>
                              <div className="hidden justify-end pr-16 sm:flex lg:pr-0">
                                   {
                                        dark ? <IconButton className=' dark:bg-bg' onClick={() => {
                                             document.documentElement.classList.toggle("dark")
                                             setDark(false)
                                             }}>
                                             <MdOutlineLightMode color='#0f0e17' size={18} />
                                        </IconButton> : <IconButton onClick={() => {
                                             document.documentElement.classList.toggle("dark")
                                             setDark(true)
                                             }}>
                                             <MdDarkMode size={18} />
                                        </IconButton>
                                   }


                              </div>
                         </div>
                    </div>
               </div>
          </header>
     );
}

const ListItem = ({ children, NavLink }) => {
     return (
          <>
               <li>
                    <Link
                         to={NavLink}
                         className="flex py-2 text-base font-medium text-dark hover:text-primary dark:text-darkPara lg:ml-10 lg:inline-flex"
                    >
                         {children}
                    </Link>
               </li>
          </>
     );
}

export default Header;