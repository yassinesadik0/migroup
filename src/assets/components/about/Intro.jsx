import React from 'react';

const Intro = () => {
     return (
          <div className='bg-bg dark:bg-darkBg'>
               <section className="mx-auto max-w-7xl px-5 py-16 md:px-10 md:py-20 overflow-hidden">
                    <div className="container">
                         <div className="text-center">
                              <h2 className="text-3xl font-extrabold md:text-5xl text-mainHeading dark:text-darkMainHeading">Qui Nous Sommes?
                              </h2>
                              <p className="mx-auto mb-8 mt-4  max-w-[800px] text-para dark:text-darkPara md:mb-12">L'INNOVATION ÉLECTRIQUE AU SERVICE D'UN AVENIR PLUS PROPRE
                              </p>
                         </div>
                         <div className="-mx-4 flex flex-wrap lg:justify-between">
                              <div className="w-full px-4 lg:w-1/2 xl:w-6/12">
                                   <div className="mb-12 max-w-[570px] lg:mb-0">
                                        <h2 className="mb-4 text-md font-normal text-dark dark:text-darkMainHeading text-para md:text-xl">
                                             Bienvenue chez GROUP D'INNOVATIONS & MAINTENENCES, votre partenaire dédié pour des solutions électriques innovantes dans les domaines du bâtiment, de l'industrie et de l'agriculture. Chez GROUP D'INNOVATIONS & MAINTENENCES, nous nous engageons à façonner un avenir énergétique durable en intégrant des technologies de pointe dans chaque projet que nous entreprenons. Notre équipe d'experts passionnés est spécialisée dans la conception et la mise en œuvre de systèmes électriques intelligents et efficaces, tant pour les constructions modernes que pour les opérations industrielles et agricoles.
                                        </h2>
                                        <p className="mb-4 text-xl font-bold  text-dark dark:text-darkMainHeading text-mainHeading md:text-4xl">
                                             ...Notre Engagement envers l'Innovation et la Durabilité
                                        </p>
                                        <h2 className="mb-4 text-md font-normal  text-dark dark:text-darkMainHeading text-para md:text-xl">
                                             Chez GROUP D'INNOVATIONS & MAINTENENCES, nous sommes bien plus qu'une entreprise d'ingénierie électrique. Nous sommes des visionnaires engagés dans la création d'un monde où l'énergie est propre, abondante et accessible à tous. En choisissant GROUP D'INNOVATIONS & MAINTENENCES, vous optez pour l'excellence, l'innovation et un engagement inébranlable envers un avenir énergétique plus vert et plus intelligent. Rejoignez-nous dans cette mission et ensemble, écrivons l'avenir de l'énergie.
                                        </h2>
                                   </div>
                              </div>
                              <div className="rounded-md w-full px-4 lg:w-1/2  ">
                                   <p className='mb-4 text-md font-normal  text-dark dark:text-darkMainHeading text-para md:text-xl'>
                                        Nous sommes fiers de notre engagement envers l'efficacité énergétique, l'optimisation des ressources et l'utilisation judicieuse des énergies renouvelables, contribuant ainsi à un environnement plus propre et plus durable. Ce qui nous distingue, c'est notre dévouement envers l'innovation. Nous repoussons sans cesse les limites de l'ingénierie électrique en adoptant les dernières avancées technologiques, notamment dans le domaine des panneaux solaires, de l'automatisation industrielle et de l'électrification agricole. Notre approche centrée sur le client nous permet de comprendre vos besoins spécifiques et de créer des solutions sur mesure qui non seulement répondent à ces besoins, mais les dépassent également.Ce qui nous distingue, c'est notre dévouement envers l'innovation. Nous repoussons sans cesse les limites de l'ingénierie électrique en adoptant les dernières avancées technologiques, notamment dans le domaine des panneaux solaires, de l'automatisation industrielle et de l'électrification agricole. Notre approche centrée sur le client nous permet de comprendre vos besoins spécifiques et de créer des solutions sur mesure qui non seulement répondent à ces besoins, mais les dépassent également.
                                   </p>
                              </div>
                         </div>
                    </div>
               </section>

          </div>
     );
}

export default Intro;
