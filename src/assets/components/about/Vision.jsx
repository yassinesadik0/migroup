import React from 'react';
import Img7 from "../../images/photo7.jpg";
import Img8 from "../../images/photo8.jpg";


const Vision = () => {
     return (
          <div>
               <section className="bg-bg dark:bg-darkBg">
                    {/* Container */}
                    <div className="mx-auto max-w-7xl px-10 py-16 ">
                         {/* Component */}
                         <div className="grid grid-cols-1 items-center gap-8  lg:grid-cols-2">
                              {/* Content Div */}
                              <div className="max-w-[720px]">
                                   <h3 className="text-3xl font-extrabold md:text-5xl text-mainHeading dark:text-darkMainHeading">Notre Vision
                                   </h3>
                                   <h3 className="mt-4  max-w-[800px] text-subHeading dark:text-darkSubHeading mb-6">UN AVENIR ALIMENTÉ PAR L'INNOVATION ÉLECTRIQUE
                                   </h3>
                                   <p className="max-w-[600px] text-md font-normal text-dark dark:text-darkPara text-para md:text-base">Chez GROUP D'INNOVATIONS & MAINTENENCES, notre vision est de façonner un avenir où l'ingénierie électrique rencontre l'innovation et la durabilité. Nous nous engageons à utiliser les technologies les plus avancées dans les secteurs industriels, agricoles et du bâtiment. Notre mission va au-delà de la simple fourniture de services électriques : nous sommes déterminés à optimiser la consommation énergétique de nos clients en intégrant des solutions intelligentes et efficaces. Nous croyons fermement en l'exploitation totale des énergies renouvelables, transformant les ressources vertes en sources d'énergie viables et respectueuses de l'environnement. Notre engagement envers un avenir durable guide chacune de nos décisions, créant ainsi un impact positif à la fois pour nos clients et pour notre planète. Rejoignez-nous dans cette passionnante aventure où la technologie rencontre la responsabilité environnementale pour façonner un monde meilleur.</p>
                              </div>
                              {/* Image Div */}
                              <div>
                                   <img src={Img7} className='rounded-md' />
                              </div>
                         </div>
                    </div>
                    <div className="mx-auto max-w-7xl  px-10 py-16">
                         {/* Component */}
                         <div className="grid grid-cols-1 items-center gap-8  lg:grid-cols-2">
                              {/* Content Div */}
                              <div>
                                   <img src={Img8} className='rounded-md' />
                              </div>
                              <div className="max-w-[720px]">
                                   <h3 className="text-3xl font-extrabold md:text-5xl text-mainHeading dark:text-darkMainHeading">Notre Mission
                                   </h3>
                                   <h3 className="mt-4  max-w-[800px] text-subHeading dark:text-darkSubHeading mb-6">CRÉER UN ENVIRONNEMENT PROPRE GRÂCE À L'ÉNERGIE SOLAIRE ET À L'OPTIMISATION ÉNERGÉTIQUE
                                   </h3>
                                   <p className="max-w-[600px] mb-4 text-md font-normal text-dark dark:text-darkPara text-para md:text-base">Chez GROUP D'INNOVATIONS & MAINTENENCES, notre mission est de contribuer à un environnement plus propre en promouvant l'utilisation d'énergies renouvelables, notamment à travers l'installation de panneaux solaires. Nous nous engageons à élaborer des solutions innovantes visant à économiser l'énergie et à optimiser les systèmes électriques et industriels. En mettant l'accent sur l'efficacité énergétique, nous aidons nos clients à réduire leur empreinte environnementale tout en réalisant des économies substantielles sur leurs coûts énergétiques. Nous croyons fermement que chaque petit geste compte dans la construction d'un avenir plus durable, et notre mission est de fournir les outils et les technologies nécessaires pour concrétiser cette vision. Rejoignez-nous dans notre engagement à créer un monde où l'énergie propre et l'innovation environnementale sont au cœur de chaque projet que nous entreprenons.</p>
                              </div>
                              {/* Image Div */}
                         </div>
                    </div>
               </section>
          </div>
     );
}

export default Vision;
