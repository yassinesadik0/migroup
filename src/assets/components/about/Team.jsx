import {
     Card,
     CardBody,
     Avatar,
     IconButton,
     Typography,
} from "@material-tailwind/react";


function TeamCard({ img, name, title }) {
     return (
          <Card className="rounded-lg bg-bg dark:bg-darkBg" shadow={false}>
               <CardBody className="text-center">
                    <Typography variant="h5" color="blue-gray" className="!font-medium text-lg text-mainHeading dark:text-darkSubHeading">
                         {name}
                    </Typography>
                    <Typography
                         color="blue-gray"
                         className="mb-2 !text-base !font-semibold text-subHeading dark:text-darkSubHeading"
                    >
                         {title}
                    </Typography>
                    <div className="flex items-center justify-center gap-1.5">
                         <IconButton variant="text" color="gray">
                              <i className="fa-brands fa-twitter text-lg" />
                         </IconButton>
                         <IconButton variant="text" color="gray">
                              <i className="fa-brands fa-linkedin text-lg" />
                         </IconButton>
                         <IconButton variant="text" color="gray">
                              <i className="fa-brands fa-dribbble text-lg" />
                         </IconButton>
                    </div>
               </CardBody>
          </Card>
     );
}


const members = [
     {
          img: `https://www.material-tailwind.com/img/avatar1.jpg`,
          name: "technique@migroup.ma",
          title: "Assistant technique",
     },
     {
          img: `https://www.material-tailwind.com/img/avatar2.jpg`,
          name: "commercial@migroup.ma",
          title: "Assistant commercial",
     },
     {
          img: `https://www.material-tailwind.com/img/avatar5.jpg`,
          name: "contact@migroup.ma",
          title: "Contact",
     }
];

export function Team() {
     return (
          <section className="py-18 px-8  bg-bg dark:bg-darkBg">
               <div className="container mx-auto">
                    <div className=" text-center ">
                         <Typography
                              variant="h1"
                              className="text-3xl font-extrabold md:text-5xl text-mainHeading dark:text-darkMainHeading"
                         >
                              Notre Équipe Principale
                         </Typography>
                         <Typography
                              variant="lead"
                              className="mx-auto mb-8 mt-4  max-w-[800px] text-para dark:text-darkPara md:mb-12"
                         >
                              Notre équipe incroyablement compétente est toujours prête à vous aider en cas de besoin.
                         </Typography>
                    </div>
                    <div className="grid grid-cols-1 gap-6 md:grid-cols-3">
                         {members.map((props, key) => (
                              <TeamCard key={key} {...props} />
                         ))}
                    </div>
               </div>
          </section>
     );
}

export default Team;