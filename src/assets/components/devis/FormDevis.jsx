import {
     Card,
     CardBody,
     Avatar,
     IconButton,
     Typography,
} from "@material-tailwind/react";
import { MdOutlineWatchLater } from "react-icons/md";


function FormDevis({ name, title }) {
     return (
          <Card className="rounded-lg bg-bg dark:bg-darkBg" shadow={false}>
               <CardBody className="text-center flex items-center gap-4">
                    <div className="w-16 h-16 flex items-center content-center">
                         <MdOutlineWatchLater color='#ffd803' size={40} />
                    </div>
                    <div>
                         <Typography variant="h5" color="blue-gray" className="!font-medium text-sm lg:text-lg text-mainHeading dark:text-darkSubHeading">
                              {name}
                         </Typography>
                         <Typography
                              color="blue-gray"
                              className="mb-2 !text-base !font-semibold text-subHeading dark:text-darkSubHeading"
                         >
                              {title}
                         </Typography>
                    </div>

               </CardBody>
          </Card>
     );
}


const members = [
     {
          name: "Lundi à vendredi",
          title: "9am à 5pm",
     },
     {
          name: "SAMEDI",
          title: "9am à 2pm",
     },
     {
          name: "DIMANCHE",
          title: "Fermé",
     }
];

export function Team() {
     return (
          <section className="py-14 px-8  bg-bg dark:bg-darkBg">
               <div className="container mx-auto">
                    <div className=" text-center ">

                         <Typography
                              variant="lead"
                              className="mx-auto font-semibold mb-8 mt-4  max-w-[800px] text-mainHeading dark:text-darkMainHeading md:mb-12"
                         >
                         Heures de travail
                         </Typography>
                    </div>
                    <div className="grid grid-cols-1 gap-6 md:grid-cols-3">
                         {members.map((props, key) => (
                              <FormDevis key={key} {...props} />
                         ))}
                    </div>
               </div>
          </section>
     );
}

export default Team;