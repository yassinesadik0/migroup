import React from 'react';
import { SlEnergy } from "react-icons/sl";
import { TbBrightnessAuto } from "react-icons/tb";
import { MdSpeed } from "react-icons/md";





const Features = () => {
     return (
          <div className='bg-bg dark:bg-darkBg'>
               {/* Section Features  */}
               <section>
                    {/* Features Container */}
                    <div className=" mx-auto max-w-7xl px-5 py-16 md:px-10 md:py-20">
                         {/* Features Title */}
                         <div className="text-center">
                              <h2 className="text-3xl font-extrabold md:text-5xl text-mainHeading dark:text-darkMainHeading">GROUP D'INNOVATIONS & MAINTENENCES, votre partenaire parfait pour les technologies récentes et efficaces</h2>
                              <p className="mx-auto mb-8 mt-4  max-w-[800px] text-para dark:text-darkPara md:mb-12">GROUP D'INNOVATIONS & MAINTENENCES, votre partenaire parfait pour les technologies récentes et efficaces; Distribution et Installation electrique et Automatisation industriel et tous qui Energétique.</p>
                         </div>
                         {/* Features Grid */}
                         <div className="grid gap-5 sm:grid-cols-2 md:grid-cols-3">
                              {/* Features Item */}
                              <div className="grid gap-8 p-8 md:p-10">
                                   <MdSpeed size={40} color='#ffd803' />

                                   <p className="text-xl font-semibold text-subHeading dark:text-darkSubHeading">DISTRIBITION ET INSTALLATION</p>
                                   <p className="text-sm text-para dark:text-darkPara">GROUP D'INNOVATIONS & MAINTENENCES assure des installations durables et rapides, garantissant des économies à long terme pour nos clients</p>
                              </div>
                              {/* Features Item */}
                              <div className="grid gap-8 p-8 md:p-10">
                                   <TbBrightnessAuto size={40} color='#ffd803' />
                                   <p className="text-xl font-semibold text-subHeading dark:text-darkSubHeading">AUTOMATISATION</p>
                                   <p className="text-sm text-para dark:text-darkPara">GROUP D'INNOVATIONS & MAINTENENCES excelle dans la rapidité, offrant des solutions automatisées efficaces et durables pour nos clients.</p>
                              </div>
                              {/* Features Item */}
                              <div className="grid gap-8 p-8 md:p-10">
                                   <SlEnergy size={35} color='#ffd803' />

                                   <p className="text-xl font-semibold text-subHeading dark:text-darkSubHeading">ENERGÉTIQUE</p>
                                   <p className="text-sm text-para dark:text-darkPara">GROUP D'INNOVATIONS & MAINTENENCES, l'expertise énergétique pour des économies durables et efficaces.</p>
                              </div>
                         </div>
                    </div>
               </section>
          </div>
     );
}

export default Features;
