import React from "react";
import { MdOutlineDesignServices } from "react-icons/md";
import { MdSettingsPower } from "react-icons/md";
import { MdOutlineSpeakerNotes } from "react-icons/md";
import { Button } from "@material-tailwind/react";
import { FaArrowRight } from "react-icons/fa6";





const ServicesPreview = () => {
     return (
          <section className=" bg-darkBg dark:bg-bg mx-auto px-5 py-16 md:px-10 md:py-20 overflow-hidden">
               <div className="container mx-auto max-w-7xl ">
                    <div className="-mx-4 flex flex-wrap">
                         <div className="w-full px-4">
                              <div className="mx-auto mb-12 max-w-[510px] text-center lg:mb-20">
                                   <h2 className="text-3xl font-extrabold md:text-5xl text-darkMainHeading dark:text-mainHeading">
                                        Services de Gestion de Projet Compréhensifs
                                   </h2>
                                   <p className="mx-auto mb-8 mt-8 max-w-[800px] text-darkPara dark:text-para md:mb-12">
                                        Optimisation de la Productivité
                                   </p>
                              </div>
                         </div>
                    </div>

                    <div className="grid gap-5 sm:grid-cols-2 md:grid-cols-3">
                         <ServiceCard
                              title="SERVICES DE A À Z"
                              details="Services complets de gestion de projet, de la conception à l'entretien. Solutions énergie renouvelable sans tracas."
                              icon={<MdOutlineDesignServices size={40} color='#ffd803' />}
                         />
                         <ServiceCard
                              title="CONFORMITÉ RÉGLEMENTAIRE"
                              details="Naviguez dans la complexité des réglementations en énergie renouvelable. Garantissez la conformité légale et les normes de sécurité."
                              icon={<MdSettingsPower size={40} color='#ffd803' />}
                         />
                         <ServiceCard
                              title="COMMUNICATION TRANSPARENTE"
                              details="Maintenez une communication claire et transparente tout au long du projet. Bâtissez la confiance et la satisfaction des clients"
                              icon={<MdOutlineSpeakerNotes size={40} color='#ffd803' />}
                         />
                    </div>
                    <a href="/services">

                    <Button className=" mx-auto mt-12 flex items-center gap-3 bg-accent text-para" size='lg'>
                         VOIR TOUTES LES PRESTATIONS
                         <FaArrowRight />
                    </Button>
                    </a>
               </div>
          </section>
     );
};

export default ServicesPreview;

const ServiceCard = ({ icon, title, details }) => {
     return (
          <>
               <div className="grid gap-8 p-8 md:p-10">
                    {icon}
                    <h4 className="text-xl font-semibold text-darkSubHeading dark:text-subHeading">
                         {title}
                    </h4>
                    <p className="text-sm text-darkPara dark:text-para">{details}</p>
               </div>
          </>
     );
};
