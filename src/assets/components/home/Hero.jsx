import React from 'react';
import { Carousel, Typography, Button } from "@material-tailwind/react";
import Img3 from "../../images/photo3.jpg"
import Img2 from "../../images/photo2.jpg"
import Img4 from "../../images/photo6.jpg"
import { FaArrowRight } from "react-icons/fa6";


const Hero = () => {
     return (
          <Carousel className="h-[90vh]">
               <div className="relative h-full w-full">
                    <img
                         src={Img3}
                         alt="image 1"
                         className="h-full w-full object-cover"
                    />
                    <div className="absolute inset-0 grid h-full w-full place-items-center bg-black/75">
                         <div className="w-3/4 text-center md:w-2/4">
                              <Typography
                                   variant="h1"
                                   color="white"
                                   className="mb-4 text-xl md:text-4xl lg:text-5xl"
                              >
                                   SERVICES D'ÉLECTRICITÉ DU BÂTIMENT
                              </Typography>
                              <Typography
                                   variant="lead"
                                   color="white"
                                   className=" text-sm md:text-xl mb-12 opacity-80"
                              >
                                   Chez GROUP D'INNOVATIONS & MAINTENENCES, nous comprenons l'importance cruciale de l'électricité dans chaque bâtiment, que ce soit pour l'éclairage, les systèmes de sécurité, ou les équipements essentiels. Notre équipe d'électriciens qualifiés est dédiée à fournir des services de qualité supérieure, garantissant la sécurité et l'efficacité de votre système électrique.
                              </Typography>
                              <div className="flex justify-center gap-2">
                              <a href="/devis">

                                   <Button className="flex items-center gap-3" color='white' size='lg'>
                                        OBTENEZ UN DEVIS
                                        <FaArrowRight />
                                   </Button>
                              </a>
                              </div>
                         </div>
                    </div>
               </div>
               <div className="relative h-full w-full">
                    <img
                         src={Img2}
                         alt="image 1"
                         className="h-full w-full object-cover"
                    />
                    <div className="absolute inset-0 grid h-full w-full place-items-center bg-black/75">
                         <div className="w-3/4 text-center md:w-2/4">
                              <Typography
                                   variant="h1"
                                   color="white"
                                   className="mb-4 text-xl md:text-4xl lg:text-5xl"
                              >
                                   SOLUTIONS D'INGÉNIERIE ÉLECTRIQUE INDUSTRIELLE
                              </Typography>
                              <Typography
                                   variant="lead"
                                   color="white"
                                   className="text-sm md:text-xl mb-12 opacity-80"
                              >
                                   Chez GROUP D'INNOVATIONS & MAINTENENCES, nous sommes leaders dans la fourniture de solutions d'ingénierie électrique industrielle sur mesure. Nous comprenons les défis uniques auxquels sont confrontées les industries, et nous sommes déterminés à offrir des solutions innovantes, fiables et efficientes pour répondre à vos besoins complexes.
                              </Typography>
                              <div className="flex justify-center gap-2">
                              <a href="/devis">

                                   <Button className="flex items-center gap-3" color='white' size='lg'>
                                        OBTENEZ UN DEVIS
                                        <FaArrowRight />
                                   </Button>
                                   </a>
                              </div>
                         </div>
                    </div>
               </div>
               <div className="relative h-full w-full">
                    <img
                         src={Img4}
                         alt="image 1"
                         className="h-full w-full object-cover"
                    />
                    <div className="absolute inset-0 grid h-full w-full place-items-center bg-black/75">
                         <div className="w-3/4 text-center md:w-2/4">
                              <Typography
                                   variant="h1"
                                   color="white"
                                   className="mb-4 text-xl md:text-4xl lg:text-5xl"
                              >
                                   AGRICULTURE ET LES ÉNERGIES RENOUVELABLES
                              </Typography>
                              <Typography
                                   variant="lead"
                                   color="white"
                                   className="text-sm md:text-xl mb-12 opacity-80"
                              >
                                   Chez GROUP D'INNOVATIONS & MAINTENENCES, nous sommes pionniers dans le domaine des solutions d'ingénierie électrique pour l'agriculture, intégrant des technologies innovantes et les énergies renouvelables pour répondre aux besoins énergétiques spécifiques de l'industrie agricole                              </Typography>
                              <div className="flex justify-center gap-2">
                              <a href="/devis">

                                   <Button className="flex items-center gap-3" color='white' size='lg'>
                                        OBTENEZ UN DEVIS
                                        <FaArrowRight />
                                   </Button>
                                   </a>
                              </div>
                         </div>
                    </div>
               </div>
          </Carousel>
     );
}

export default Hero;